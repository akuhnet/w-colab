###
![](https://1.bp.blogspot.com/-y9Y3RURi3wg/YUSbxRPX4gI/AAAAAAAAD6w/Mcfb5Mm_64cJ2XffBHUYTRSix8QDOAu4ACLcBGAsYHQ/s0/RDP%2BColab%2B%25281%2529.gif)
Don't close tab colab to keep rdp running 12 hours
## __Connect With RDP__
[![Typing SVG](https://readme-typing-svg.herokuapp.com?color=16D400&size=25&width=770&lines=Free+RDP+Google+Colab)](https://git.io/typing-svg)
- [Windows](https://www.akuh.net/2021/08/free-rdp-colab.html)
- [LXDE](https://www.akuh.net/2021/09/lxde-desktop-on-google.html)
- [XFCE4](https://colab.research.google.com/github/akuhnet/w-colab/blob/main/xrdp.ipynb)

###
###
[![Typing SVG](https://readme-typing-svg.herokuapp.com?color=16D400&size=25&width=770&lines=Free+RDP+Google+Cloud)](https://git.io/typing-svg)

- [Fedora](https://www.akuh.net/2021/09/free-rdp-google-cloud-fedora.html)
- [Centos 7](https://www.akuh.net/2021/09/free-rdp-google-cloud-centos.html)
- [Centos 8](https://www.akuh.net/2021/09/free-rdp-google-cloud-centos.html)
- [Ubuntu](https://www.akuh.net/2021/09/free-rdp-ubuntu-2004.html)
- [Kali Linux](https://www.akuh.net/2021/09/free-rdp-kali-linux.html)


Add Katacoda

![](https://1.bp.blogspot.com/-PhecerKh-8M/YVARPcEH_oI/AAAAAAAAECg/jJ-n7C1XtxMNQzBU6231VKwbtUSpS5m3wCLcBGAsYHQ/s894/free%2B4h.jpg)


###
## __Tutorials__

1. [__Google Colab RDP__](https://www.akuh.net/2021/08/free-rdp-colab.htm)
1. [__Google Colab VNC__](https://www.akuh.net/2021/06/lifetime-google-colaboratory.html)
1. [__Free Rdp Google Cloud__](https://www.akuh.net/2021/05/vps-google-cloud-free-lifetime-update.html)
1. [__Other free vps__](https://www.akuh.net/search/label/Vps)

###
###

